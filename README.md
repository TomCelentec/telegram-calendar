# Telegram Calendar

Retrieving ics calendar and sending today's data to a telegram bot

## Script version

### Installation
Requirements : `pip3 install requests ics python-telegram-bot`

### Program execution
`python3 telegram-calendar.py`

## Docker version
For the docker version to work, you need to copy a valid configuration file into the docker container.
Container creation: `docker run --name telegram-calendar ccservereu/telegram-calendar`.
Configuration file copy: `docker cp ./config.json telegram-calendar:/home/`.

## Licence
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
