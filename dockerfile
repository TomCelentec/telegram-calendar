FROM python:3-alpine
MAINTAINER Clément COTE <tom.celentec@protonmail.com>

ENV TZ=Europe/Paris

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk update && apk upgrade

ADD . /home/

WORKDIR /home/

RUN pip3 install requests ics python-telegram-bot

RUN crontab /home/crontab

CMD ["crond", "-f"]