import json
import sys

import requests
import telegram

from calendarParse import calendar_parse, telegram_string_generation


def main():
    with open("config.json", "r") as f:
        config_data_string = f.read()
        config_data = json.loads(config_data_string)

    if (config_data["Telegram-token"]=="XXX") or (config_data["Calendar-URL"]=="https://xxx") or (config_data["Chat-ID"]=="xxx"):
        sys.stderr.write("Please change the configuration file: config.json\n")
        return

    calendar_web_data = requests.get(config_data["Calendar-URL"])

    today_events = calendar_parse(calendar_web_data, config_data["Timezone"])

    telegram_string = telegram_string_generation(today_events)

    telegram_bot = telegram.Bot(token=config_data["Telegram-token"])

    telegram_bot.sendMessage(config_data["Chat-ID"], telegram_string, parse_mode="MarkdownV2")


if __name__ == "__main__":
    main()
