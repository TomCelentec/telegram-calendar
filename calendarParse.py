import datetime

import pytz
from ics import Calendar


def partition(arr, low, high):
    i = (low - 1)  # index of smaller element
    pivot = arr[high]["beginTime"]  # pivot

    for j in range(low, high):

        # If current element is smaller than or
        # equal to pivot
        if arr[j]["beginTime"] <= pivot:
            # increment index of smaller element
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return i + 1


def quick_sort(arr, low, high):
    if len(arr) == 1:
        return arr
    if low < high:
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low, high)

        # Separately sort elements before
        # partition and after partition
        quick_sort(arr, low, pi - 1)
        quick_sort(arr, pi + 1, high)


def calendar_parse(calendar_web_data, timezone_name):
    calendar_data = Calendar(calendar_web_data.text)

    timezone = pytz.timezone(timezone_name)

    today_events = []

    for event in calendar_data.events:
        begin_date_time = event.begin.datetime.astimezone(timezone)
        if begin_date_time.date() == datetime.date.today():
            event_dict = {
                "beginTime": begin_date_time,
                "endTime": event.end.datetime.astimezone(timezone),
                "duration": event.duration,
                "name": event.name,
                "description": event.description,
                "location": event.location
            }
            today_events.append(event_dict)

    quick_sort(today_events, 0, len(today_events) - 1)

    return today_events


def telegram_string_generation(today_events):
    string_events = []

    if len(today_events) > 0:
        for i in range(len(today_events)):
            hour = today_events[i]["duration"].total_seconds() // 3600
            minutes = (today_events[i]["duration"].total_seconds() - 3600 * hour) // 60

            event_string = "*" + today_events[i]["name"].replace("(", "\(").replace(")", "\)") + "*\n"

            event_string = event_string + "De " + today_events[i]["beginTime"].time().isoformat(timespec="minutes")
            event_string = event_string + " à " + today_events[i]["endTime"].time().isoformat(timespec="minutes")
            event_string = event_string + " \({:02.0f}h{:02.0f}\)\n\n".format(hour, minutes)

            if today_events[i]["location"] is not None:
                event_string = event_string + today_events[i]["location"].replace("(", "\(").replace(")", "\)") + "\n\n"

            description = today_events[i]["description"].split("\n")

            formateurs = []

            for j in range(len(description)):
                if "Formateurs" in description[j]:
                    j += 1
                    while len(description[j]) != 0:
                        formateurs.append(description[j].replace(" -", "").replace("(", "\(").replace(")", "\)"))
                        j += 1

            event_string = event_string + "Formateurs :" + ",".join(formateurs)

            event_string = event_string.replace("-", "\-")
            event_string = event_string.replace(".", "\.")

            string_events.append(event_string)

        telegram_string = "__*" + datetime.date.today().strftime("%A %d %B %Y") + "*__"

        telegram_string = telegram_string + "\n\n\n" + "\n\n\n".join(string_events)

    else:
        telegram_string = "__*" + datetime.date.today().strftime("%A %d %B %Y") + "*__"

        telegram_string = telegram_string + "\n\n" + "Pas de cours aujourd'hui"

    return telegram_string
